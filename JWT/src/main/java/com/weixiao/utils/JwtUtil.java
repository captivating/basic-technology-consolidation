package com.weixiao.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtil {

    /**
     * 过期时间24小时
     */
    private static final long EXPIRE_TIME = 24 * 60 * 60 * 1000;

    public static String createToken(String username, String password, Integer userid) {
        //过期时间
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        //私钥及加密算法
        Algorithm algorithm = Algorithm.HMAC256(password);
        //设置头信息
        Map<String, Object> header = new HashMap<>(2);
        header.put("typ", "JWT");
        header.put("alg", "HS256");
        //附带username和userID生成签名
        return JWT.create().withHeader(header).withClaim("userId", userid)
                .withClaim("username", username).withExpiresAt(date).sign(algorithm);
    }
}