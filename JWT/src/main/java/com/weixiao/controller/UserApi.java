package com.weixiao.controller;

import cn.hutool.json.JSONObject;
import com.weixiao.entity.User;
import com.weixiao.note.PassToken;
import com.weixiao.utils.JwtUtil;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("api")
public class UserApi {
    /*@Autowired
    UserService userService;*/

    //登录
    @PassToken
    @PostMapping("/login")
    public Object login() {
        //此user模拟登录时用户传的信息
        User user = new User(1, "韩诚", "hc19990914");

        //User userForBase=userService.findByUsername(user);
        //此userForBase模拟根据用户姓名获取到数据库的用户信息
        User userForBase = new User(1, "韩诚", "hc19990914");
        //验证数据库中是否有此用户并验证密码
        Assert.notNull(userForBase, "用户名或者密码错误");
        Assert.isTrue(userForBase.getPassword().equals(user.getPassword()), "用户名或者密码错误");

        //创建token
        String token = JwtUtil.createToken(userForBase.getUsername(), userForBase.getPassword(), userForBase.getUserId());
        HashMap<Object, Object> map = new HashMap<>(3);
        map.put("token",token);
        map.put("user",userForBase);
        return map;
    }

    @GetMapping("/getMessage")
    public String getMessage() {
        return "你已通过验证";
    }
}
