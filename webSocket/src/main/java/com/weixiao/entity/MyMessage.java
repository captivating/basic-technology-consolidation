package com.weixiao.entity;

import lombok.Data;

/**
 * @Author 韩诚
 * @Date 2023/11/13 19:55
 */
@Data
public class MyMessage {
    private Long userId;
    private String message;
}
