package com.weixiao.config;
 
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
@Configuration
public class MapperFacotoryAutoWire {
    @Bean
    public MapperFacade mapperFactory() {
        return new DefaultMapperFactory.Builder().build().getMapperFacade();
    }
}