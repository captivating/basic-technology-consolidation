package com.weixiao.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * author 韩诚
 * date 2022/12/13
 */
@Data
public class PuserPageListVO {
    @ApiModelProperty("用户ID")
    private Integer id;

    @ApiModelProperty("用户名称")
    private String name;

    @ApiModelProperty("父ID")
    private Integer pId;
}
