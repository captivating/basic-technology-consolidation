package com.weixiao.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * author 韩诚
 * date 2022/12/13
 */
@Data
public class UserPageListVO {
    @ApiModelProperty("用户ID")
    private Integer userId;

    @ApiModelProperty("用户名称")
    private String userName;

    @ApiModelProperty("用户性别")
    private String userSex;

    @ApiModelProperty("用户年龄")
    private Integer userAge;

    @ApiModelProperty("用户密码")
    private String password;
}
