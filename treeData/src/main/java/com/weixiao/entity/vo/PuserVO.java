package com.weixiao.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * author 韩诚
 * date 2022/12/12
 */
@Data
public class PuserVO {
    @ApiModelProperty("用户ID")
    private Integer id;

    @ApiModelProperty("用户名称")
    private String name;

    @ApiModelProperty("父ID")
    private Integer pId;

    @ApiModelProperty("子用户列表")
    private List<PuserVO> puserChildList;
}
