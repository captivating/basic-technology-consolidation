package com.weixiao.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * author 韩诚
 * date 2022/12/13
 */
@Data
public class PageDTO implements Serializable {

    @ApiModelProperty(value = "查询页码", example = "1")
    @NotNull(message = "查询页码不能为空")
    private Integer pageNo = 1;

    @ApiModelProperty(value = "查询条数", example = "10")
    @NotNull(message = "查询条数不能为空")
    private Integer pageSize = 10;

}
