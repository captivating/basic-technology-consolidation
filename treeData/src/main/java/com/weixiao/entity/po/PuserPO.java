package com.weixiao.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Puser)表实体类
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */

@Data
@TableName("puser")
@ApiModel(value = "Puser对象", description = "用户表")
public class PuserPO implements Serializable {

    @ApiModelProperty("用户ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("父ID")
    @TableField("p_id")
    private Integer pId;

}

