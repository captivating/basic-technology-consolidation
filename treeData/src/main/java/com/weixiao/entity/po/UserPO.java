package com.weixiao.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (User)表实体类
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
@ApiModel(value = "User对象", description = "用户表")
@TableName("user")
@Data
public class UserPO {

    @ApiModelProperty("用户ID")
    @TableId(value = "user_id",type = IdType.AUTO)
    private Integer userId;

    @ApiModelProperty("用户名称")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty("用户性别")
    @TableField("user_sex")
    private String userSex;

    @ApiModelProperty("用户年龄")
    @TableField("user_age")
    private Integer userAge;

    @ApiModelProperty("用户密码")
    @TableField("password")
    private String password;
}

