package com.weixiao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weixiao.entity.dto.UserPageListDTO;
import com.weixiao.entity.po.UserPO;
import com.weixiao.entity.vo.UserPageListVO;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
public interface UserService extends IService<UserPO> {

    /**
     * 分页查询
     * @param userPageListDTO
     * @return
     */
    List<UserPageListVO> selectPageList(UserPageListDTO userPageListDTO);
}

