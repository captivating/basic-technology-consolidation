package com.weixiao.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.weixiao.dao.PuserDao;
import com.weixiao.entity.dto.PuserPageListDTO;
import com.weixiao.entity.po.PuserPO;
import com.weixiao.entity.vo.PuserPageListVO;
import com.weixiao.entity.vo.PuserVO;
import com.weixiao.service.PuserService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import xin.altitude.cms.common.util.TreeUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (Puser)表服务实现类
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
@Service("puserService")
public class PuserServiceImpl extends ServiceImpl<PuserDao, PuserPO> implements PuserService {
    @Resource
    private MapperFacade mapperFacade;

    /**
     * 获取树型结构数据（手写）
     * @return
     */
    @Override
    public List<PuserVO> getTreeDataDefault() {
        //获取所有数据并转化成VO
        List<PuserPO> puserPOList = baseMapper.selectList(null);
        List<PuserVO> puserVOS = mapperFacade.mapAsList(puserPOList, PuserVO.class);
        //创建一个list集合用来存放最终返回数据
        List<PuserVO> puserVOList=new ArrayList<>();
        //遍历数据
        for (PuserVO puserVO : puserVOS) {
            //如果为顶级数据，直接放进list集合中
            if (puserVO.getPId() == 0) {
                puserVOList.add(puserVO);
            }
            //创建一个list集合用来存放当前数据的子数据
            List<PuserVO> puserChildList = new ArrayList<>();
            //遍历所有数据
            for (PuserVO puserVOChild : puserVOS) {
                //如果数据的父ID为当前数据的ID，则存入到子list中
                if (puserVOChild.getPId() .equals(puserVO.getId()) ) {
                    puserChildList.add(puserVOChild);
                }
            }
            //将子list集合注入到当前数据的子数据属性中
            puserVO.setPuserChildList(puserChildList);
        }
        //将获得的树形结构数据返回到前端
        return puserVOList;
    }

    /**
     * 获取树型结构数据（Hutool工具包）
     * @return
     */
    @Override
    public List<PuserVO> getTreeDataHutool() {
        //获取所有数据并转化成VO
        List<PuserPO> puserPOList = baseMapper.selectList(null);
        List<PuserVO> puserVOList = mapperFacade.mapAsList(puserPOList, PuserVO.class);

        //转为树结构
        TreeNodeConfig config = new TreeNodeConfig();

        //config.setDeep(20);//最大递归深度  默认无限制
        // config可以配置属性字段名和排序等等
        config.setParentIdKey("pid");
        config.setChildrenKey("puserChildList");
        List<Tree<Integer>> treeNodes = TreeUtil.build(puserVOList, 0, config, (object, tree) -> {
            tree.setId(object.getId());//必填属性
            tree.setParentId(object.getPId());//必填属性
            tree.setName(object.getName());
            // 扩展属性 ...
            //tree.putExtra("children",object.getChildren());
        });

        //将树型结构JSON转换成JSON字符串
        String s = JSON.toJSONString(treeNodes);
        //将JSON字符串转换成对象集合
        List<PuserVO> puserList = JSON.parseArray(s, PuserVO.class);

        //返回数据给前端
        return puserList;
    }

    /**
     * 获取树型结构数据(依赖)
     * @return
     */
    @Override
    public List<PuserVO> getTreeDataTreeUtils() {
        //获取所有数据并转化成VO
        List<PuserPO> puserPOList = baseMapper.selectList(null);
        List<PuserVO> puserVOList = mapperFacade.mapAsList(puserPOList, PuserVO.class);
        List<PuserVO> puserVOListTree = TreeUtils.createNodeDetail(puserVOList, 0, PuserVO::getId, PuserVO::getPId, PuserVO::getPuserChildList);
        return puserVOListTree;
    }

    /**
     * 分页查询
     * @param puserPageListDTO
     * @return
     */
    @Override
    public List<PuserPageListVO> selectPageList(PuserPageListDTO puserPageListDTO) {
        //分页
        PageHelper.startPage(puserPageListDTO.getPageNo(), puserPageListDTO.getPageSize());
        //查询出符合条件的所有数据
        List<PuserPO> puserPOList = baseMapper.selectList(new LambdaQueryWrapper<PuserPO>()
                .eq(ObjectUtil.isNotNull(puserPageListDTO.getId()), PuserPO::getId, puserPageListDTO.getId())
                .like(StringUtils.isNotBlank(puserPageListDTO.getName()), PuserPO::getName, puserPageListDTO.getName())
                .eq(ObjectUtil.isNotNull(puserPageListDTO.getPId()), PuserPO::getPId, puserPageListDTO.getPId()));
        //将数据转为VO并返回
        return mapperFacade.mapAsList(puserPOList, PuserPageListVO.class);
    }
}