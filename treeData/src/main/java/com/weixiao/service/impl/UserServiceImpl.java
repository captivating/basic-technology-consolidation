package com.weixiao.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.weixiao.dao.UserDao;
import com.weixiao.entity.dto.UserPageListDTO;
import com.weixiao.entity.po.UserPO;
import com.weixiao.entity.vo.UserPageListVO;
import com.weixiao.service.UserService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserPO> implements UserService {
    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public List<UserPageListVO> selectPageList(UserPageListDTO userPageListDTO) {
        //分页
        PageHelper.startPage(userPageListDTO.getPageNo(),userPageListDTO.getPageSize());
        //查出所有符合条件的数据
        List<UserPO> userPOList = baseMapper.selectList(new LambdaQueryWrapper<UserPO>()
                .eq(ObjectUtil.isNotNull(userPageListDTO.getUserId()), UserPO::getUserId, userPageListDTO.getUserId())
                .like(StringUtils.isNotBlank(userPageListDTO.getUserName()), UserPO::getUserName, userPageListDTO.getUserName())
                .eq(StringUtils.isNotBlank(userPageListDTO.getUserSex()), UserPO::getUserSex, userPageListDTO.getUserSex())
                .eq(ObjectUtil.isNotNull(userPageListDTO.getUserAge()), UserPO::getUserAge, userPageListDTO.getUserAge())
                .like(StringUtils.isNotBlank(userPageListDTO.getPassword()), UserPO::getPassword, userPageListDTO.getPassword()));
        //将数据转为VO并返回
        return mapperFacade.mapAsList(userPOList, UserPageListVO.class);
    }
}

