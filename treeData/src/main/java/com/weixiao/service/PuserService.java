package com.weixiao.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.weixiao.entity.dto.PuserPageListDTO;
import com.weixiao.entity.po.PuserPO;
import com.weixiao.entity.vo.PuserPageListVO;
import com.weixiao.entity.vo.PuserVO;

import java.util.List;

/**
 * (Puser)表服务接口
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
public interface PuserService extends IService<PuserPO> {

    /**
     * 获取树型结构数据（手写）
     * @return
     */
    List<PuserVO> getTreeDataDefault();

    /**
     * 获取树型结构数据（Hutool工具包）
     * @return
     */
    List<PuserVO> getTreeDataHutool();

    /**
     * 获取树型结构数据(依赖)
     * @return
     */
    List<PuserVO> getTreeDataTreeUtils();

    /**
     * 分页查询
     * @param puserPageListDTO
     * @return
     */
    List<PuserPageListVO> selectPageList(PuserPageListDTO puserPageListDTO);

}

