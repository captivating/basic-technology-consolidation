package com.weixiao.controller;


import cn.hutool.core.lang.tree.Tree;
import com.weixiao.entity.dto.PuserPageListDTO;
import com.weixiao.entity.po.PuserPO;
import com.weixiao.entity.vo.PuserPageListVO;
import com.weixiao.entity.vo.PuserVO;
import com.weixiao.service.PuserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Puser)表控制层
 *
 * @author makejava
 * @since 2022-12-12 15:48:07
 */
@Api(tags = "用户管理（父子关系）")
@RestController
@RequestMapping("puser")
public class PuserController{
    /**
     * 服务对象
     */
    @Resource
    private PuserService puserService;

    /**
     * 获取树型结构数据(手写)
     * @return
     */
    @ApiOperation(value = "获取树型结构数据(手写)",httpMethod = "GET")
    @GetMapping("getTreeDataDefault")
    public List<PuserVO> getTreeDataDefault() {
        return puserService.getTreeDataDefault();
    }

    /**
     * 获取树型结构数据(Hutool工具包)
     * @return
     */
    @ApiOperation(value = "获取树型结构数据(Hutool工具包)",httpMethod = "GET")
    @GetMapping("getTreeDataHutool")
    public List<PuserVO> getTreeDataHutool() {
        return puserService.getTreeDataHutool();
    }

    /**
     * 获取树型结构数据(依赖)
     * @return
     */
    @ApiOperation(value = "获取树型结构数据(依赖)",httpMethod = "GET")
    @GetMapping("getTreeDataTreeUtils")
    public List<PuserVO> getTreeDataTreeUtils() {
        return puserService.getTreeDataTreeUtils();
    }

    /**
     * 分页查询所有数据
     *
     * @param puserPageListDTO 查询条件
     * @return 符合条件的所有数据
     */
    @ApiOperation(value = "分页查询",httpMethod = "POST")
    @PostMapping("selectPageList")
    public List<PuserPageListVO> selectPageList(PuserPageListDTO puserPageListDTO) {
        return puserService.selectPageList(puserPageListDTO);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据",httpMethod = "GET")
    @GetMapping("{id}")
    public PuserPO selectOne(@PathVariable Serializable id) {
        return puserService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param puserPO 实体对象
     * @return 新增结果
     */
    @ApiOperation(value = "新增数据",httpMethod = "POST")
    @PostMapping("add")
    public Boolean insert(@RequestBody PuserPO puserPO) {
        return puserService.save(puserPO);
    }

    /**
     * 修改数据
     *
     * @param puserPO 实体对象
     * @return 修改结果
     */
    @ApiOperation(value = "修改数据",httpMethod = "PUT")
    @PutMapping("edit")
    public Boolean update(@RequestBody PuserPO puserPO) {
        return puserService.updateById(puserPO);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiOperation(value = "删除数据",httpMethod = "DELETE")
    @DeleteMapping("del")
    public boolean delete(@RequestParam("idList") List<Long> idList) {
        return puserService.removeByIds(idList);
    }
}

