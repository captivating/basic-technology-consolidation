package com.weixiao.controller;


import com.weixiao.entity.dto.UserPageListDTO;
import com.weixiao.entity.po.UserPO;
import com.weixiao.entity.vo.UserPageListVO;
import com.weixiao.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 分页查询所有数据
     *
     * @param userPageListDTO 查询条件
     * @return 所有数据
     */
    @ApiOperation(value = "分页查询",httpMethod = "POST")
    @PostMapping("selectPageList")
    public List<UserPageListVO> selectPageList(UserPageListDTO userPageListDTO) {
        return userService.selectPageList(userPageListDTO);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据",httpMethod = "GET")
    @GetMapping("{id}")
    public UserPO selectOne(@PathVariable Serializable id) {
        return userService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param userPO 实体对象
     * @return 新增结果
     */
    @ApiOperation(value = "新增数据",httpMethod = "POST")
    @PostMapping
    public Boolean insert(@RequestBody UserPO userPO) {
        return userService.save(userPO);
    }

    /**
     * 修改数据
     *
     * @param userPO 实体对象
     * @return 修改结果
     */
    @ApiOperation(value = "修改数据",httpMethod = "PUT")
    @PutMapping
    public Boolean update(@RequestBody UserPO userPO) {
        return userService.updateById(userPO);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiOperation(value = "删除数据",httpMethod = "DELETE")
    @DeleteMapping
    public Boolean delete(@RequestParam("idList") List<Long> idList) {
        return userService.removeByIds(idList);
    }
}

