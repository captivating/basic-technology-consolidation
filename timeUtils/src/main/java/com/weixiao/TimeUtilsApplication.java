package com.weixiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeUtilsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeUtilsApplication.class, args);
    }

}
