package com.weixiao.ServiceImpl;

import com.weixiao.entity.UserPO;
import org.apache.commons.lang3.time.DateUtils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * author 韩诚
 * date 2023/2/23 11:20
 */
public class TimeDemo {
    public static void main(String[] args) throws Exception {
        //创建对象，给字段赋值
        UserPO userPO = new UserPO();
        userPO.setCreateTime("2023-02-23");

        //调用工具类
        updateTime(UserPO.class, userPO);

        //输出对象，看时间是否已经改变
        System.out.println(userPO);
    }

    /**
     * 使用枚举实现一个方法，能够入参不同对象，一般用来实现不同对象，处理相同逻辑业务
     *
     * @param clazz 入参对象的类
     * @param cla   入参对象
     * @param <T>
     */
    public static <T> void updateTime(Class<T> clazz, T cla) throws Exception {
        //获取到入参的对象
        T t = clazz.cast(cla);

        //获取对象的指定字段
        Field field = t.getClass().getDeclaredField("createTime");
        //获取对象所有字段，当需要操作属性多时，便获取所有字段进行遍历，再使用字段名称与所需要操作的字段名进行equals
        //Field[] declaredFields = t.getClass().getDeclaredFields();

        //放开权限，否则private修饰的字段值获取不到
        field.setAccessible(true);

        //获取到对象字段的值
        Object o = field.get(t);
        System.out.println("修改前字段值： " + o);

        //修改对象字段的值
        field.set(t, o + " 00:00:00");
        System.out.println("修改后字段值： " + field.get(t));
    }


    /**
     * 获取近七天日期
     */
    public static List<String> getSevenDate() {
        List<String> dateList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 6; i >= 0; i--) {
            Date date = DateUtils.addDays(new Date(), -i);
            String formatDate = sdf.format(date);
            dateList.add(formatDate);
        }
        return dateList;
    }

    /**
     * 获取本月到目前所有日期（yyyy-mm-dd格式字符串）
     *
     * @return
     */
    public static List<String> getMonthFullDay() {
        SimpleDateFormat dateFormatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        List<String> fullDayList = new ArrayList<>();
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        for (int j = 1; j <= getWeekMonthYear(); j++) {
            fullDayList.add(dateFormatYYYYMMDD.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }

    /**
     * 获取本年到目前所有月份
     *
     * @return
     */
    public static List<String> getYearFullMonth() {

        ArrayList<String> stringList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 0);
        for (int i = 0; i < getMonthByYear(); i++) {
            stringList.add(sdf.format(cal.getTime()));
            cal.add(Calendar.MONTH, 1);
        }
        return stringList;
    }

    /**
     * 判断当前是本月第几天
     *
     * @return
     */
    public static int getWeekMonthYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前为本年第几个月
     *
     * @return
     */
    public static int getMonthByYear() {
        return (Calendar.getInstance().get(Calendar.MONTH) + 1);
    }

    /**
     * 获取时间段内的所有日期
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static List<String> findDates(String startTime, String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dStart = null;
        Date dEnd = null;
        try {
            dStart = sdf.parse(startTime);
            dEnd = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cStart = Calendar.getInstance();
        cStart.setTime(dStart);

        List dateList = new ArrayList();
        dateList.add(sdf.format(dStart.getTime()));
        // 此日期是否在指定日期之后
        while (dEnd.after(cStart.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            cStart.add(Calendar.DAY_OF_MONTH, 1);
            Date time = cStart.getTime();
            String format = sdf.format(time);
            dateList.add(format);
        }
        return dateList;
    }

}
