package com.weixiao.entity;

import lombok.Data;

/**
 * author 韩诚
 * date 2023/2/23 11:21
 */
@Data
public class UserPO {
    private Integer id;
    private String name;
    private String sex;
    private Integer age;
    private String createTime;
}
