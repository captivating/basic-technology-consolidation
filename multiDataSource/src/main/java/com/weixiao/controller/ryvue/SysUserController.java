package com.weixiao.controller.ryvue;


import com.github.pagehelper.PageInfo;
import com.weixiao.entity.ryvue.SysUser;
import com.weixiao.entity.ssm.Puser;
import com.weixiao.service.ryvue.SysUserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 用户信息表(SysUser)表控制层
 *
 * @author makejava
 * @since 2022-12-27 10:47:15
 */
@RestController
@RequestMapping("sysUser")
public class SysUserController {
    /**
     * 服务对象
     */
    @Resource
    private SysUserService sysUserService;

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/getPageList")
    public PageInfo<SysUser> getPageList(Integer pageNum, Integer pageSize){
        return sysUserService.getPageList(pageNum,pageSize);
    }

    /**
     * 通过sql语句查询
     * @param userId
     * @return
     */
    @RequestMapping("/selectByUserId")
    public SysUser selectByUserId(Long userId){
        return sysUserService.selectByUserId(userId);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public SysUser selectOne(@PathVariable Serializable id) {
        return sysUserService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param sysUser 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Boolean insert(@RequestBody SysUser sysUser) {
        return sysUserService.save(sysUser);
    }

    /**
     * 修改数据
     *
     * @param sysUser 实体对象
     * @return 修改结果
     */
    @PutMapping("edit")
    public Boolean update(@RequestBody SysUser sysUser) {
        return sysUserService.updateById(sysUser);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("del")
    public boolean delete(@RequestParam("idList") List<Long> idList) {
        return sysUserService.removeByIds(idList);
    }
}

