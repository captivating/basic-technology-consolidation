package com.weixiao.controller.ssm;

import com.github.pagehelper.PageInfo;
import com.weixiao.entity.ssm.Puser;
import com.weixiao.service.ssm.PuserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Puser)表控制层
 *
 * @author makejava
 * @since 2022-12-27 10:43:55
 */
@RestController
@RequestMapping("puser")
public class PuserController{
    /**
     * 服务对象
     */
    @Resource
    private PuserService puserService;

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/getPageList")
    public PageInfo<Puser> getPageList(Integer pageNum, Integer pageSize){
        return puserService.getPageList(pageNum,pageSize);
    }


    /**
     * 通过sql语句查询
     * @param puserId
     * @return
     */
    @RequestMapping("/selectByPuserId")
    public Puser selectByPuserId(Integer puserId){
        return puserService.selectByPuserId(puserId);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public Puser selectOne(@PathVariable Serializable id) {
        return puserService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param puser 实体对象
     * @return 新增结果
     */
    @PostMapping("add")
    public Boolean insert(@RequestBody Puser puser) {
        return puserService.save(puser);
    }

    /**
     * 修改数据
     *
     * @param puser 实体对象
     * @return 修改结果
     */
    @PutMapping("edit")
    public Boolean update(@RequestBody Puser puser) {
        return puserService.updateById(puser);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("del")
    public boolean delete(@RequestParam("idList") List<Long> idList) {
        return puserService.removeByIds(idList);
    }
}