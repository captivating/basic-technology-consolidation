package com.weixiao.config;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

import com.github.pagehelper.PageInterceptor;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@MapperScan(basePackages = "com.weixiao.dao.ssm", sqlSessionFactoryRef = "ssmSqlSessionFactory")
public class SsmDataSourceConfig {

    @Primary // 表示这个数据源是默认数据源, 这个注解必须要加，因为不加的话spring将分不清楚那个为主数据源（默认数据源）
    @Bean("ssmDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.ssm") //读取application.yml中的配置参数映射成为一个对象
    public DataSource getSsmDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean("ssmSqlSessionFactory")
    public SqlSessionFactory ssmSqlSessionFactory(@Qualifier("ssmDataSource") DataSource dataSource) throws Exception {
        MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        // mapper的xml形式文件位置必须要配置，不然将报错：no statement （这种错误也可能是mapper的xml中，namespace与项目的路径不一致导致）
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mappers/ssm/*.xml"));

        //此处创建一个Configuration 注意包不要引错了
        org.apache.ibatis.session.Configuration configuration=new org.apache.ibatis.session.Configuration();
        //配置日志实现
        configuration.setLogImpl(StdOutImpl.class);
        //此处可以添加其他mybatis配置 例如转驼峰命名
        configuration.setMapUnderscoreToCamelCase(true);

        //分页插件
        Interceptor[] interceptors = new Interceptor[1];
        interceptors[0] = new PageInterceptor();

        Properties properties = new Properties();
        //开启分页判断：分页页数大于总页数时显示最后一页，小于等于0时显示第一页，默认关闭
        properties.setProperty("reasonable","true");

        interceptors[0].setProperties(properties);
        bean.setPlugins(interceptors);

        return bean.getObject();
    }

    @Primary
    @Bean("ssmSqlSessionTemplate")
    public SqlSessionTemplate ssmSqlSessionTemplate(@Qualifier("ssmSqlSessionFactory") SqlSessionFactory sqlSessionFactory){
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
