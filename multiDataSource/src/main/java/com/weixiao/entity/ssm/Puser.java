package com.weixiao.entity.ssm;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Puser)表实体类
 *
 * @author makejava
 * @since 2022-12-27 10:43:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Puser {

    @TableId("id")
    private Integer id;

    private String name;

    private Integer pId;

}

