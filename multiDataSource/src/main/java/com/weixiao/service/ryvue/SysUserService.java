package com.weixiao.service.ryvue;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.weixiao.entity.ryvue.SysUser;

/**
 * 用户信息表(SysUser)表服务接口
 *
 * @author makejava
 * @since 2022-12-27 10:47:15
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 通过sql语句查询
     * @param userId
     * @return
     */
    SysUser selectByUserId(Long userId);

    PageInfo<SysUser> getPageList(Integer pageNum, Integer pageSize);
}

