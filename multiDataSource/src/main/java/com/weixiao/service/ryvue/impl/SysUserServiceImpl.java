package com.weixiao.service.ryvue.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.weixiao.dao.ryvue.SysUserDao;
import com.weixiao.entity.ryvue.SysUser;
import com.weixiao.entity.ssm.Puser;
import com.weixiao.service.ryvue.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户信息表(SysUser)表服务实现类
 *
 * @author makejava
 * @since 2022-12-27 10:47:15
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserService {

    @Resource
    private SysUserDao sysUserDao;
    /**
     * 通过sql语句查询
     * @param userId
     * @return
     */
    @Override
    public SysUser selectByUserId(Long userId) {
        return sysUserDao.selectByUserId(userId);
    }

    @Override
    public PageInfo<SysUser> getPageList(Integer pageNum, Integer pageSize) {
            PageHelper.startPage(pageNum,pageSize);
            List<SysUser> sysUserList = baseMapper.selectList(null);
            PageInfo<SysUser> pageInfo = new PageInfo<>(sysUserList);
            return pageInfo;
    }
}

