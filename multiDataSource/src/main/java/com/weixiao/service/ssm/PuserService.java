package com.weixiao.service.ssm;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.weixiao.entity.ssm.Puser;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * (Puser)表服务接口
 *
 * @author makejava
 * @since 2022-12-27 10:43:56
 */
public interface PuserService extends IService<Puser> {

    Puser selectByPuserId(Integer puserId);

    PageInfo<Puser> getPageList(Integer pageNum, Integer pageSize);
}

