package com.weixiao.service.ssm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.weixiao.dao.ssm.PuserDao;
import com.weixiao.entity.ssm.Puser;
import com.weixiao.service.ssm.PuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Puser)表服务实现类
 *
 * @author makejava
 * @since 2022-12-27 10:43:56
 */
@Service("puserService")
public class PuserServiceImpl extends ServiceImpl<PuserDao, Puser> implements PuserService {
    @Resource
    private PuserDao puserDao;

    @Override
    public Puser selectByPuserId(Integer puserId) {
        return puserDao.selectByPuserId(puserId);
    }

    @Override
    public PageInfo<Puser> getPageList(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Puser> pusers = baseMapper.selectList(null);
        PageInfo<Puser> pageInfo = new PageInfo<>(pusers);
        return pageInfo;
    }
}

