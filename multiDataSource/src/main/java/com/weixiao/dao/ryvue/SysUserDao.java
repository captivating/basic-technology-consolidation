package com.weixiao.dao.ryvue;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weixiao.entity.ryvue.SysUser;

/**
 * 用户信息表(SysUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-12-27 10:47:15
 */
public interface SysUserDao extends BaseMapper<SysUser> {

    SysUser selectByUserId(Long userId);
}

