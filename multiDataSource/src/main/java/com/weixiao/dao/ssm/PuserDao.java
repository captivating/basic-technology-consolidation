package com.weixiao.dao.ssm;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weixiao.entity.ssm.Puser;

/**
 * (Puser)表数据库访问层
 *
 * @author makejava
 * @since 2022-12-27 10:43:56
 */
public interface PuserDao extends BaseMapper<Puser> {
    Puser selectByPuserId(Integer puserId);
}

