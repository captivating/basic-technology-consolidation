package com.weixiao.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.weixiao.entity.User;

public class MemberExcelListener extends AnalysisEventListener<User> {

	@Override
	public void invoke(User user, AnalysisContext analysisContext) {
		System.out.println("读取User=" + user);
	}

	//所有的invoke执行结束，最后再执行这个方法（此方法只在最后结束时执行一次）
	@Override
	public void doAfterAllAnalysed(AnalysisContext analysisContext) {
		System.out.println("读取Excel完毕");
	}
}