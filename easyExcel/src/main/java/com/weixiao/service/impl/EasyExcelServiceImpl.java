package com.weixiao.service.impl;

import com.weixiao.entity.User;
import com.weixiao.service.EasyExcelService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * author 韩诚
 * date 2022/12/13
 */
@Service
public class EasyExcelServiceImpl implements EasyExcelService {
    @Override
    public List<User> getUserAll() {
        // 这里构造一些测试数据，具体业务场景可从数据库等其他地方获取
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setName("韩诚");
        user.setSex("男");
        user.setAge(22);
        list.add(user);

        User user1 = new User();
        user1.setName("李端生");
        user1.setSex("男");
        user1.setAge(23);
        list.add(user1);

        User user2 = new User();
        user2.setName("何政");
        user2.setSex("男");
        user2.setAge(23);
        list.add(user2);

        return list;
    }
}
