package com.weixiao.service;

import com.weixiao.entity.User;

import java.util.List;

/**
 * author 韩诚
 * date 2022/12/13
 */
public interface EasyExcelService {
    List<User> getUserAll();
}
