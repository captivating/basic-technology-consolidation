package com.weixiao.stream;

import cn.hutool.core.util.ObjectUtil;
import com.weixiao.stream.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * author 韩诚
 * date 2022/12/14
 */
public class ListGather {
    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();
        User user = new User(1, "韩诚", 22, "男");
        userList.add(user);
        User user0 = new User(1, "韩诚", 22, "男");
        userList.add(user0);
        User user1 = new User(2, "何超", 23, "男");
        userList.add(user1);
        User user2 = new User(3, "李端生", 23, "男");
        userList.add(user2);
        User user3 = new User(4, "徐佩", 21, "女");
        userList.add(user3);
        User user4 = new User(5, "陈佳宏", 21, "女");
        userList.add(user4);
        User user5 = new User(6, "吴紫璇", 22, "女");
        userList.add(user5);

        System.out.println("userList :  " + userList);
        //去重
        List<User> newUserList = userList.stream().distinct().collect(Collectors.toList());
        System.out.println("newUserList : " + newUserList);

        //获取集合中对象的某个属性集合(年龄)
        //List<Integer> userAgeList = userList.stream().map(x->x.getAge()).collect(Collectors.toList());
        //使用方法引用   User::getAge
        List<Integer> userAgeList = userList.stream().map(User::getAge).collect(Collectors.toList());
        System.out.println("userAgeList : " + userAgeList);

        //将list集合以id为key，对象为value转为map集合
        //Map<Integer, User> userMap = newUserList.stream().collect(Collectors.toMap(o -> o.getId(), o -> o));
        //使用方法引用   User::getId
        Map<Integer, User> userMap = newUserList.stream().collect(Collectors.toMap(User::getId, o -> o));
        System.out.println("userMap : " + userMap);

        //过滤器(过滤掉名字叫韩诚的)
        List<User> userListFilter = userList.stream().filter(userFilter -> !userFilter.getName().equals("韩诚")).collect(Collectors.toList());
        System.out.println("userListFilter : " + userListFilter);

        //将一个list集合进行分页，pageNo为页数，pageSize为每页条数
        int pageNo = 1;
        int pageSize = 5;
        //获取集合第一页数据，每页五条数据
        List<User> pageList = userList.stream()
                .skip((pageNo - 1) * pageSize).limit(pageSize)
                .collect(Collectors.toList());
        System.out.println("获取的分页数据 : "+pageList);
    }
}
