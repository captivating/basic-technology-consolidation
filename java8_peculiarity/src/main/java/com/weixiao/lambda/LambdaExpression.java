package com.weixiao.lambda;

import com.weixiao.lambda.interfacetest.*;

/**
 * author 韩诚
 * date 2022/12/15
 *
 * Lambda简介：Lambda 表达式是 JDK8 的一个新特性，可以取代大部分的匿名内部类
 *
 * 对接口的要求：
 * 虽然使用 Lambda 表达式可以对某些接口进行简单的实现，但并不是所有的接口都可以使用 Lambda 表达式来实现。Lambda 规定接口中只能有一个需要被实现的方法，不是规定接口中只能有一个方法
 *
 * jdk 8 中有另一个新特性：
 * default， 被 default 修饰的方法会有默认实现，不是必须被实现的方法，所以不影响 Lambda 表达式的使用。
 *
 * @FunctionalInterface 修饰函数式接口的，要求接口中的抽象方法只有一个（校验接口中抽象方法个数，大于 1 会报错） 这个注解往往会和 lambda 表达式一起出现。
 */
public class LambdaExpression {
    public static void main(String[] args) {
        //无参无返回
        NoReturnNoParam noReturnNoParam = () -> System.out.println("无参无返回");
        noReturnNoParam.method();

        System.out.println();

        //一个参数无返回
        NoReturnOneParam noReturnOneParam = a -> System.out.println("一个参数无返回:" + a);
        noReturnOneParam.method(6);

        System.out.println();

        //多个参数无返回
        NoReturnMultiParam noReturnMultiParam = (a, b) -> System.out.println("多个参数无返回:" + "{" + a + "," + +b + "}");
        noReturnMultiParam.method(6, 8);

        System.out.println();

        //无参有返回值
        ReturnNoParam returnNoParam = () -> 1;
        System.out.println("无参有返回值: " + returnNoParam.method());

        System.out.println();

        //一个参数有返回值
        ReturnOneParam returnOneParam = a -> a + 3;
        System.out.println("一个参数有返回值:" + returnOneParam.method(6));

        System.out.println();

        //多个参数有返回值
        ReturnMultiParam returnMultiParam = (a, b) -> a + b;
        System.out.println("多个参数有返回值:" + returnMultiParam.method(6, 8));
    }
}
