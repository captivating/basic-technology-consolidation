package com.weixiao.lambda.interfacetest;

/**无参无返回值*/
@FunctionalInterface
public interface NoReturnNoParam {
    void method();
}