package com.weixiao.lambda.interfacetest;

/*** 无参有返回*/
@FunctionalInterface
public interface ReturnNoParam {
    int method();
}