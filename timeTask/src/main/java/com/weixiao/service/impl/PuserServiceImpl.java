package com.weixiao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weixiao.dao.PuserDao;
import com.weixiao.entity.Puser;
import com.weixiao.service.PuserService;
import com.weixiao.timeTask.MyTimeTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Timer;

/**
 * (Puser)表服务实现类
 *
 * @author makejava
 * @since 2023-01-07 16:42:09
 */
@Service
public class PuserServiceImpl extends ServiceImpl<PuserDao, Puser> implements PuserService {
    @Resource
    private PuserDao puserDao;

    @Override
    public void getPuserById(Integer id) {
        System.out.println("定时任务开启，5秒后拿到ID为 "+id+" 的用户信息");
        //创建定时器
        Timer timer = new Timer();
        //创建需要执行的任务
        MyTimeTask myTimeTask = new MyTimeTask(puserDao,id);
        //定时器执行此任务，延迟时间5秒
        timer.schedule(myTimeTask,1000*5);
    }
}

