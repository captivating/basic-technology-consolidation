package com.weixiao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weixiao.entity.Puser;

/**
 * (Puser)表服务接口
 *
 * @author makejava
 * @since 2023-01-07 16:42:09
 */
public interface PuserService extends IService<Puser> {

    void getPuserById(Integer id);
}

