package com.weixiao.controller;


import com.weixiao.service.PuserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("puser")
public class PuserController{
    @Resource
    private PuserService puserService;

    @GetMapping("{id}")
    public void selectOne(@PathVariable Integer id) {
        puserService.getPuserById(id);
    }
}

