package com.weixiao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weixiao.entity.Puser;

/**
 * (Puser)表数据库访问层
 *
 * @author makejava
 * @since 2023-01-07 16:42:09
 */
public interface PuserDao extends BaseMapper<Puser> {

}

