package com.weixiao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling//定时任务
@MapperScan(basePackages = "com.weixiao.dao")
@SpringBootApplication
public class TimeTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeTaskApplication.class, args);
    }

}
