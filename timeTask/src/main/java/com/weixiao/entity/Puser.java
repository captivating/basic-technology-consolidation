package com.weixiao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Puser{

    @TableId("id")
    private Integer id;

    private String name;

    private Integer pId;
}

