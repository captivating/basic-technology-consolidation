package com.weixiao.timeTask;

import com.weixiao.dao.PuserDao;
import com.weixiao.entity.Puser;

import java.util.TimerTask;

/**
 * author 韩诚
 * date 2023/01/07
 */

public class MyTimeTask extends TimerTask {
    private PuserDao puserDao;
    private Integer id;

    //puserDao不能自动注入，只能通过这种方式注入
    public MyTimeTask(PuserDao puserDao,Integer id) {
        this.puserDao=puserDao;
        this.id=id;
    }

    //执行延迟任务
    @Override
    public void run() {
        //获取指定ID的数据
        Puser puser = puserDao.selectById(id);
        System.out.println("ID为 "+id+" 的用户信息： "+puser);
    }
}
