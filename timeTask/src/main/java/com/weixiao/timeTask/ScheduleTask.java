package com.weixiao.timeTask;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * author 韩诚
 * date 2023/01/07
 */
@Component
public class ScheduleTask {

    /**
     * @Scheduled(initialDelay = 5000) //第一次被调用前延迟执行时间
     * @Scheduled(fixedDelay = 5000) //上一次   执行完毕时间点   之后5秒再执行
     * @Scheduled(fixedRate = 5000)  //上一次   开始执行时间点   之后5秒再执行
     * @Scheduled(cron="0/5 * *  * * ? ") //通配符
     */
    @Scheduled(cron="0/5 * *  * * ? ")   //每5秒执行一次
    public void execute(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //设置日期格式
        System.out.println("当前时间： " + df.format(new Date()));
    }
}
