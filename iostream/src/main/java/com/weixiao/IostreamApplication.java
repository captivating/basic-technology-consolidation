package com.weixiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IostreamApplication {

    public static void main(String[] args) {
        SpringApplication.run(IostreamApplication.class, args);
    }

}
