package com.weixiao.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * author 韩诚
 * date 2022/12/23
 */
@RestController
@RequestMapping("iostream")
public class IOStreamController {
    @RequestMapping("/getFile")
    public void getFile(HttpServletResponse response) throws IOException {
        //获取到需要读取的文件
        File file = new File("C:\\Users\\gxzn\\Pictures\\Saved Pictures\\杀生丸.jpg");
        //创建输入流
        FileInputStream inputStream = new FileInputStream(file);
        //获取输出流
        OutputStream outputStream = response.getOutputStream();

        //开始读取并输出
        byte[] b = new byte[1024];
        int length;
        while ((length = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, length);
        }
    }
}