package com.weixiao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weixiao.entity.User;

/**
 * (User)表数据库访问层
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
public interface UserDao extends BaseMapper<User> {

}

