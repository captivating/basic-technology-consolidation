package com.weixiao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weixiao.entity.User;

import java.util.List;
import java.util.Map;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2022-12-12 15:48:08
 */
public interface UserService extends IService<User> {

    /**
     * 查询并返回List集合
     * @param user
     * @return
     */
    List<User> selectUserList(User user);

    /**
     * 查询并返回Map集合
     * @param user
     * @return
     */
    Map<Integer,User> selectUserMap(User user);

    /**
     * 修改数据
     * @param user
     */
    void edit(User user);
}

